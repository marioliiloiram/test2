import Vue from 'vue'
import '../theme/index.css'
import { DatePicker,Dialog,Button,Loading,Row,Col,Icon,Tooltip,Popover,Table,TableColumn,Carousel,CarouselItem,Pagination,Input,Form,FormItem,Select,Option,Checkbox,
    CheckboxButton,
    CheckboxGroup, } from 'element-ui'
Vue.use(DatePicker)
Vue.use(Dialog)
Vue.use(Button)
Vue.use(Loading)
Vue.use(Row)
Vue.use(Col)
Vue.use(Icon)
Vue.use(Tooltip)
Vue.use(Popover)
Vue.use(Table)
Vue.use(TableColumn)
Vue.use(Carousel)
Vue.use(CarouselItem)
Vue.use(Pagination)
Vue.use(Input)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Select)
Vue.use(Option)
Vue.use(Checkbox)
Vue.use(CheckboxButton)
Vue.use(CheckboxGroup)