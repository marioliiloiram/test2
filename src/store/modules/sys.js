import api from '@/api'
import router from '@/router'

const formatMenu = (menus) => {
    let newMenus = {}
    if (menus != false) {
        var Prefix = "m"
        menus.forEach(function(menu) {
            let cur = {
                Title: menu.Title,
                TitleEn: menu.TitleEn,
                Icon: menu.Icon,
                Action: menu.Action,
                Blank: menu.Blank
            }
            if (menu.Pid == 0) {
                if (newMenus[Prefix + menu.Id]) {
                    newMenus[Prefix + menu.Id] = Object.assign(newMenus[Prefix + menu.Id], cur)
                } else {
                    cur["items"] = {}
                    newMenus[Prefix + menu.Id] = cur
                }
            } else {
                if (newMenus[Prefix + menu.Pid]) {
                    if (!newMenus[Prefix + menu.Pid]["items"][menu.Group]) {
                        newMenus[Prefix + menu.Pid]["items"][menu.Group] = []
                    }
                } else {
                    newMenus[Prefix + menu.Pid] = {}
                    newMenus[Prefix + menu.Pid]["items"] = {}
                    newMenus[Prefix + menu.Pid]["items"][menu.Group] = []
                }
                newMenus[Prefix + menu.Pid]["items"][menu.Group].push(cur)
            }
            //要不要处理没有 子菜单的
        })
    }
    return newMenus
}
const state = {
    title: '皇朝娱乐',
    menus: [],
    boards: [],
    spotlights: [],
    config: Object,
    ps:[]
}
const actions = {
    title: ({ commit }, b) => {
        commit('title', b);
    },
    async ['MENU']({ commit, state }) {
        if (state.menus.length == 0) {
            let data = await api.get("/pc/menus")
            commit("setMenus", data.menu)
            state.ps = data.ps
        }
    },
    async ['BOARD']({ commit, state }) {
        if (state.boards.length == 0) {
            let boards = await api.get("/pc/boards")
            commit("setBoards", boards)
        }
    },
    async ['SPOTLIGHTS']({ commit, state }) {
        if (state.spotlights.length == 0) {
            let spotlights = await api.get("/pc/spotlights")
            commit("setSpotlights", spotlights)
        }
    },
    async ['CONFIGURE']({ commit, state }) {
        if (state.config == Object) {
            let config = await api.get("/pc/configure")
            commit("setConfig", config)
            commit("setTitle", config["title"])

        }
    },
    userCenter: ({ commit }, b) => {
        commit('ShowCentre', b)
        router.push("/home")
    }
}

const mutations = {
    setTitle(state, title) {
        state.title = title
    },
    setMenus(state, menus) {
        state.menus = formatMenu(menus)
    },
    setBoards(state, boards) {
        state.boards = boards
    },
    setSpotlights(state, spotlights) {
        state.spotlights = spotlights
    },
    setConfig(state, b) {
        state.config = b
    }
}

const getters = {
    ['menus'](state) { return state.menus },
    ['title'](state) { return state.title },
    ['boards'](state) { return state.boards },
    ['spotlights'](state) { return state.spotlights },
    ['config'](state) { return state.config },
    ['ps'](state) { return state.ps }
}

export default {
    actions,
    state,
    mutations,
    getters
}