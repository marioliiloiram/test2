import '@babel/polyfill'
import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import store from './store/'
import router from './router'
import * as custom from './filters/custom'
import './element-ui/element-ui.js'


// Vue.config.debug = false
// Vue.config.devtools = false
// Vue.config.productionTip = false

/* eslint-disable */
Object.keys(custom).forEach(key => {
    Vue.filter(key, custom[key])
})

new Vue({
    store,
    router,
    render: h => h(App)
}).$mount('#app')