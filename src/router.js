import Vue from 'vue'
import Router from 'vue-router'
// import Index from './views/Index.vue'
// import Index from './views/index1.vue'
import Index from './views/index2.vue'
import Lottery from './views/Lotto.vue'
import Sports from './views/Sports.vue'
import Live from './views/Live.vue'
import Slot from './views/Slot.vue'
import Play from './views/Play.vue'
import Home from './views/user/Home.vue'
import Activity from './views/Activity.vue'
import Login from './views/Login.vue'
// import Help from './views/help/Help.vue'
import Help from './views/help/Help1.vue'
import store from './store'
import Notify from './views/Notify.vue'
import Caipiao from './views/Caipiao.vue'
import Zhenren from './views/Zhenren.vue'
import Laohuji from './views/Laohuji.vue'

Vue.use(Router)

const router = new Router({
    base: '/',
    routes: [{
            path: '/',
            name: 'index',
            component: Index
        },
        {
            path: '/home',
            name: 'home',
            component: Home,
            meta: {
                requireAuth: true
            }
        },
        {
            path: '/play/:id',
            component: Play,
            meta: {
                requireAuth: true
            }
        },
        {
            path: '/lottery',
            name: 'lottery',
            component: Lottery,
            meta: {
                requireAuth: true
            }
        },
        {
            path: '/ultimate',
            name: 'ultimate',
            component: Lottery,
            meta: {
                requireAuth: true
            }
        },
        {
            path: '/caipiao',
            name: 'caipiao',
            component: Caipiao
        },
        {
            path: '/sports',
            name: 'sports',
            component: Sports
        },
        {
            path: '/live',
            name: 'live',
            component: Zhenren
        },
        {
            path: '/slot',
            name: 'slot',
            component: Laohuji
        },
        {
            path: '/as/:token',
            name: 'login',
            component: Login
        },
        {
            path: '/activity',
            name: 'activity',
            component: Activity
        },
        {
            path: '/help',
            name: 'help',
            component: Help
        },
        {
            path: '/help/:id',
            name: 'ihelp',
            component: Help
        },
        {
            path: '/affreg',
            name: 'apply',
            component: Index
        },
        {
            path: '/notify/:id',
            name: 'notify',
            component: Notify
        }
    ]
})

router.beforeEach((to, from, next) => {
    if (to.matched.some(r => r.meta.requireAuth)) {
        if (store.getters.token || window.localStorage.getItem('token')) {
            next()
        } else {
            store.commit("ShowGlobal", "login")
            next({path: from.path})
        }
    } else {
        next()
    }
})

export default router