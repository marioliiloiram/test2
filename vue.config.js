const path = require('path')
module.exports = {
    baseUrl: '/',
    outputDir: 'dist',
    chainWebpack: (config) => {
        config.module
        .rule('vue')
        .use('vue-loader')
        .loader('vue-loader')
        .tap(options => {return options})
    },
    configureWebpack: (config) => {
        if (process.env.NODE_ENV === 'production') {
            config.devtool =  false
        }
    },
    productionSourceMap:false,
    devServer: {
        open: process.platform === 'darwin',
        host: '0.0.0.0',
        port: 9981,
        https: false,
        hotOnly: false,
        proxy: {
            '/': {
                target: 'http://localhost:1240',
                pathRewrite: { '^/api': '/' },
                xfwd: true,
                ws:false
            },
        },
        // before: require('./mock'),
    }
}